package com.bigfans.systemservice.service;


import com.bigfans.systemservice.model.Role;

import java.util.List;

public interface RoleService {

    List<Role> listRolesByUser(String userId) throws Exception;

}
