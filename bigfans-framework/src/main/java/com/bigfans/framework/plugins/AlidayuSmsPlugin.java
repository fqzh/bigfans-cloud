package com.bigfans.framework.plugins;

import com.bigfans.framework.exception.ServiceRuntimeException;
import com.bigfans.framework.utils.JsonUtils;
import com.bigfans.framework.utils.StringHelper;
import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class AlidayuSmsPlugin implements SmsPlugin {

	private Logger logger = LoggerFactory.getLogger(AlidayuSmsPlugin.class);

	@Override
	public void plugin() {
	}

	@Override
	public void unplugin() {

	}

	private String url;
	private String appKey;
	private String secret;
	private String signName;
	private String template_valideCode;
	private String template_prodArrival;
	private String template_priceDown;

	public void setTemplate_prodArrival(String template_prodArrival) {
		this.template_prodArrival = template_prodArrival;
	}

	public void setTemplate_valideCode(String template_valideCode) {
		this.template_valideCode = template_valideCode;
	}

	public void setTemplate_priceDown(String template_priceDown) {
		this.template_priceDown = template_priceDown;
	}

	public AlidayuSmsPlugin(String url, String appKey, String secret , String signName) {
		this.url = url;
		this.appKey = appKey;
		this.secret = secret;
		this.signName = signName;
	}

	@Override
	public String sendVerificationCode(String mobile , String vcode) {
		TaobaoClient client = new DefaultTaobaoClient(url, appKey, secret);
		AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
		req.setExtend("normal");
		req.setSmsType("normal");
		req.setSmsFreeSignName(signName);
		req.setSmsParamString("");
		req.setRecNum(mobile);
		req.setSmsTemplateCode(template_valideCode);
		
		Map<String , Object> params = new HashMap<>();
		params.put("validCode", vcode);
		req.setSmsParamString(JsonUtils.toJsonString(params));
		
		AlibabaAliqinFcSmsNumSendResponse rsp = null;
		try {
			rsp = client.execute(req);
		} catch (ApiException e) {
			throw new ServiceRuntimeException(e);
		}
		return rsp == null ? null : rsp.getBody();
	}

	@Override
	public String sendProdArrivalNotice(String mobile) {
		TaobaoClient client = new DefaultTaobaoClient(url, appKey, secret);
		AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
		req.setExtend("normal");
		req.setSmsType("normal");
		req.setSmsFreeSignName(signName);
		req.setSmsParamString("");
		req.setRecNum(mobile);
		req.setSmsTemplateCode(template_prodArrival);
		AlibabaAliqinFcSmsNumSendResponse rsp = null;
		try {
			rsp = client.execute(req);
		} catch (ApiException e) {
			throw new ServiceRuntimeException(e);
		}
		return rsp == null ? null : rsp.getBody();
	}

	@Override
	public String sendPriceDownNotice(String mobile) {
		TaobaoClient client = new DefaultTaobaoClient(url, appKey, secret);
		AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
		req.setExtend("normal");
		req.setSmsType("normal");
		req.setSmsFreeSignName(signName);
		req.setSmsParamString("");
		req.setRecNum(mobile);
		req.setSmsTemplateCode(template_priceDown);
		AlibabaAliqinFcSmsNumSendResponse rsp = null;
		try {
			rsp = client.execute(req);
		} catch (ApiException e) {
			throw new ServiceRuntimeException(e);
		}
		return rsp == null ? null : rsp.getBody();
	}
}
