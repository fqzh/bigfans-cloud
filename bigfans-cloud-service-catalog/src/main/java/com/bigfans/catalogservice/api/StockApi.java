package com.bigfans.catalogservice.api;

import com.bigfans.catalogservice.model.Stock;
import com.bigfans.catalogservice.service.sku.StockLogService;
import com.bigfans.catalogservice.service.sku.StockService;
import com.bigfans.framework.Applications;
import com.bigfans.framework.CurrentUser;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author lichong
 * @create 2018-04-11 下午7:48
 **/
@RestController
public class StockApi  extends BaseController{

    @Autowired
    private StockService stockService;
    @Autowired
    private StockLogService stockLogService;

    @GetMapping(value = "stock")
    public RestResponse getStock(@RequestParam(value = "prodId") String prodId) throws Exception{
        Stock stock = stockService.getByProd(prodId);
        return RestResponse.ok(stock.getRest());
    }


    @PostMapping(value = "/orderStockOut")
    public RestResponse orderStockOut(
            @RequestParam(value = "id") String orderId,
            @RequestBody Map<String, Integer> prodQuantityMap
    ) throws Exception {
        CurrentUser currentUser = Applications.getCurrentUser();
        Integer count = stockLogService.countByOrder(orderId);
        if(count > 0 ){
            return RestResponse.ok();
        }
        stockService.orderStockOut(orderId, currentUser.getUid(), prodQuantityMap);
        return RestResponse.ok();
    }
}
