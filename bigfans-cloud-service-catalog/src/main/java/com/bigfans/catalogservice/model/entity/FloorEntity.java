package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 
 * @Description:产品楼层,用于首页显示 1F:xxxx,2F:xxxx
 * @author lichong 2015年5月14日上午10:50:23
 *
 */
@Data
@Table(name="Floor")
public class FloorEntity extends AbstractModel {

	private static final long serialVersionUID = 1L;
	@Column(name="title")
	protected String title;
	@Column(name="background_img")
	protected String backgroundImg;
	@Column(name="order_num")
	protected Integer orderNum;
	@Column(name="category_id")
	protected String categoryId;
	
	public String getModule() {
		return "Floor";
	}

}
