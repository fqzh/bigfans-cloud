package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.AttributeValue;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;



/**
 * 
 * @Title: 
 * @Description: 属性值DAO操作
 * @author lichong 
 * @date 2015年12月21日 上午11:14:57 
 * @version V1.0
 */
public interface AttributeValueDAO extends BaseDAO<AttributeValue> {

	List<AttributeValue> listByAttribute(String attrId);
	
	List<AttributeValue> listByProduct(String productId);

	List<AttributeValue> listByProductGroup(String pgId);

	List<AttributeValue> listById(List<String> idList);
}
