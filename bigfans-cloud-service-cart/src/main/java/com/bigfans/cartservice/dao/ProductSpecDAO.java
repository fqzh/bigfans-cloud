package com.bigfans.cartservice.dao;

import com.bigfans.cartservice.model.ProductSpec;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;

/**
 * 
 * @Title: 
 * @Description: 商品和规格关联表DAO
 * @author lichong 
 * @date 2015年12月21日 下午9:55:11 
 * @version V1.0
 */
public interface ProductSpecDAO extends BaseDAO<ProductSpec> {

	List<ProductSpec> listByProdId(String prodId);
	
}
