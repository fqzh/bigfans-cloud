package com.bigfans.cartservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.api.clients.ServiceRequest;
import com.bigfans.framework.CurrentUser;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.web.RequestHolder;
import com.bigfans.model.dto.cart.*;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lichong
 * @create 2018-02-15 下午6:54
 **/
@Component
public class PricingServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "calculateCartFallback")
    public CartPricingResultDto calculateCart(CurrentUser currentUser , CartPricingDto cartPricingDto){
        ServiceRequest serviceRequest = new ServiceRequest(restTemplate, currentUser);
        Map data = serviceRequest.post(Map.class , "http://pricing-service/calculateCart" , cartPricingDto);
        CartPricingResultDto calculateResultDto = new CartPricingResultDto();
        calculateResultDto.setTotalPrice(BigDecimal.valueOf((Double) data.get("totalPrice")));
        calculateResultDto.setOriginalTotalPrice(BigDecimal.valueOf((Double) data.get("originalTotalPrice")));
        Map priceMap = (Map)data.get("priceMap");
        for(Object key : priceMap.keySet()){
            Map priceItem = (Map)priceMap.get(key);
            CartItemPricingResultDto itemPricingResultDto = BeanUtils.mapToModel(priceItem, CartItemPricingResultDto.class);
            calculateResultDto.addItemResult(itemPricingResultDto);
        }

        Map couponMap = (Map)data.get("couponMap");
        for(Object key : couponMap.keySet()){
            List couponItems = (List) couponMap.get(key);
            for(Object couponItem : couponItems){
                CartItemCouponDto cartItemCouponDto = BeanUtils.mapToModel((Map) couponItem, CartItemCouponDto.class);
                calculateResultDto.addCoupon((String) key , cartItemCouponDto);
            }
        }

        Map promotionMap = (Map)data.get("promotionMap");
        for(Object key : promotionMap.keySet()){
            List promotionItems = (List)promotionMap.get(key);
            for(Object pmtItem : promotionItems){
                CartItemPromotionDto cartItemPromotionDto = BeanUtils.mapToModel((Map) pmtItem, CartItemPromotionDto.class);
                calculateResultDto.addPromotion((String) key , cartItemPromotionDto);
            }
        }
        return calculateResultDto;
    }

    public CartPricingResultDto calculateCartFallback(CurrentUser currentUser , CartPricingDto cartPricingDto){
        CartPricingResultDto dto = new CartPricingResultDto();
        dto.setPriceMap(new HashMap<>());
        return dto;
    }

}
