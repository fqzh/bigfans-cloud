package com.bigfans.model.event.payment;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;

/**
 * @author lichong
 * @create 2018-03-15 上午7:32
 **/
@Data
public class PayMethodChangedEvent extends AbstractEvent{

    private String methodId;

    public PayMethodChangedEvent(String methodId) {
        this.methodId = methodId;
    }
}
