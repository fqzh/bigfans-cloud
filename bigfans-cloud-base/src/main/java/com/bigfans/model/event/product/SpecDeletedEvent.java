package com.bigfans.model.event.product;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lichong
 * @create 2018-05-05 上午7:37
 **/
@Data
@NoArgsConstructor
public class SpecDeletedEvent extends AbstractEvent {

    private String specId;

    public SpecDeletedEvent(String specId) {
        this.specId = specId;
    }
}
