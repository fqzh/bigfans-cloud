package com.bigfans.userservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Table;

@Data
@Table(name = "UserPoint_Log")
public class UserPointLogEntity extends AbstractModel {

    public static final String REASON_ORDERCREATE = "ordercreate";
    public static final String REASON_ORDERREVERT = "orderrevert";

    private String userId;
    private Float points;
    private String orderId;
    private String direction;
    private String reason;

    @Override
    public String getModule() {
        return "UserPointLog";
    }
}
