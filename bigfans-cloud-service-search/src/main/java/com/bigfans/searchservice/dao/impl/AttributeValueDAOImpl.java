package com.bigfans.searchservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.searchservice.dao.AttributeValueDAO;
import com.bigfans.searchservice.model.AttributeValue;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * @Title: 
 * @Description: 属性值DAO操作
 * @author lichong 
 * @date 2015年12月21日 上午11:14:57 
 * @version V1.0
 */
@Repository(AttributeValueDAOImpl.BEAN_NAME)
public class AttributeValueDAOImpl extends MybatisDAOImpl<AttributeValue> implements AttributeValueDAO {
	
	public static final String BEAN_NAME = "attributeValueDAO";
	
	@Override
	public List<AttributeValue> listByProduct(String productId) {
		ParameterMap params = new ParameterMap();
		params.put("productId", productId);
		return getSqlSession().selectList(className + ".listByProduct", params);
	}

	@Override
	public List<AttributeValue> listById(List<String> idList) {
		ParameterMap params = new ParameterMap();
		params.put("idList", idList);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<AttributeValue> listByAttribute(String optionId) {
		ParameterMap params = new ParameterMap();
		params.put("optionId", optionId);
		return getSqlSession().selectList(className + ".list", params);
	}

}
